/********************************************************************************
** Form generated from reading UI file 'serverwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERVERWIDGET_H
#define UI_SERVERWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ServerWidget
{
public:
    QLabel *serverMessage;
    QLineEdit *txtPort;
    QProgressBar *serverProgressBar;
    QPushButton *btnStart;
    QLabel *lblClient;

    void setupUi(QWidget *ServerWidget)
    {
        if (ServerWidget->objectName().isEmpty())
            ServerWidget->setObjectName(QStringLiteral("ServerWidget"));
        ServerWidget->resize(348, 216);
        serverMessage = new QLabel(ServerWidget);
        serverMessage->setObjectName(QStringLiteral("serverMessage"));
        serverMessage->setGeometry(QRect(40, 70, 81, 21));
        txtPort = new QLineEdit(ServerWidget);
        txtPort->setObjectName(QStringLiteral("txtPort"));
        txtPort->setGeometry(QRect(120, 30, 141, 21));
        serverProgressBar = new QProgressBar(ServerWidget);
        serverProgressBar->setObjectName(QStringLiteral("serverProgressBar"));
        serverProgressBar->setGeometry(QRect(40, 100, 271, 21));
        serverProgressBar->setValue(24);
        btnStart = new QPushButton(ServerWidget);
        btnStart->setObjectName(QStringLiteral("btnStart"));
        btnStart->setGeometry(QRect(230, 130, 91, 31));
        lblClient = new QLabel(ServerWidget);
        lblClient->setObjectName(QStringLiteral("lblClient"));
        lblClient->setGeometry(QRect(40, 30, 71, 16));

        retranslateUi(ServerWidget);

        QMetaObject::connectSlotsByName(ServerWidget);
    } // setupUi

    void retranslateUi(QWidget *ServerWidget)
    {
        ServerWidget->setWindowTitle(QApplication::translate("ServerWidget", "ServerWidget", nullptr));
        serverMessage->setText(QApplication::translate("ServerWidget", "\350\257\267\351\200\211\346\213\251\346\226\207\344\273\266", nullptr));
        txtPort->setText(QApplication::translate("ServerWidget", "12345", nullptr));
        btnStart->setText(QApplication::translate("ServerWidget", "\345\274\200\345\247\213\347\233\221\345\220\254", nullptr));
        lblClient->setText(QApplication::translate("ServerWidget", "\346\234\215\345\212\241\345\231\250\347\253\257\345\217\243", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ServerWidget: public Ui_ServerWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERVERWIDGET_H
