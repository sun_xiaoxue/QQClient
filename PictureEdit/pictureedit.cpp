#include "pictureedit.h"
#include "ui_pictureedit.h"
#include "photoshotdialog.h"

PictureEdit::PictureEdit(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PictureEdit)
{
    ui->setupUi(this);

    //裁剪头像活动窗口
    m_psDialog = new PhotoShotDialog(ui->label);
    m_psDialog->show();

    //加载图片
    loadPicture();
}

PictureEdit::~PictureEdit()
{
    delete ui;
}

void PictureEdit::loadPicture()
{
    m_srcPix.load("D:\\work\\workspace\\qtstudyspace\\PictureEdit\\images\\1.png");
    m_scaledPix = m_srcPix.scaled(ui->label->width(), ui->label->height());
    ui->label->setPixmap(m_scaledPix);
}

void PictureEdit::on_pushButton_clicked()
{
    //截图
    QPixmap pix = m_scaledPix.copy(m_psDialog->getCutGeometry());

    //
    pix.save("D:\\work\\workspace\\qtstudyspace\\PictureEdit\\images\\cut.png", "png");
}
