#include "photoshotdialog.h"
#include "cutdialog.h"
#include <QDebug>

PhotoShotDialog::PhotoShotDialog(QWidget *parent)
	:QWidget(parent)
{
	setWindowFlags(Qt::FramelessWindowHint);

	dialog = new CutDialog(this);
	dialog->show();
    setGeometry(0, 0, parentWidget()->width(), parentWidget()->height());
    connect(dialog, SIGNAL(signalMoved()), this, SLOT(sltCutDlgMoved()));
}

PhotoShotDialog::~PhotoShotDialog(void)
{

}

void PhotoShotDialog::sltCutDlgMoved()
{
    Q_EMIT signalMoved(dialog->geometry());
}

void PhotoShotDialog::paintEvent(QPaintEvent */*e*/)
{
	QPainterPath painterPath;
	QPainterPath p;
    p.addRect(x(),y(),rect().width(),rect().height()); //裁剪头像活动窗口
    painterPath.addRect(dialog->geometry());  //头像剪切窗口
    QPainterPath drawPath = p.subtracted(painterPath);

	QPainter paint(this);
    paint.setOpacity(0.7); //设置Qt控件的透明度， 0：表示全透明；1：表示不透明
	paint.fillPath(drawPath,QBrush(Qt::black));
}
