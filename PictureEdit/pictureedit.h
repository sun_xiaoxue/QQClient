#ifndef PICTUREEDIT_H
#define PICTUREEDIT_H

#include <QMainWindow>
#include <QPixmap>
#include "photoshotdialog.h"

namespace Ui {
class PictureEdit;
}

//头像裁剪窗口
class PictureEdit : public QMainWindow
{
    Q_OBJECT

public:
    explicit PictureEdit(QWidget *parent = 0);
    ~PictureEdit();

private slots:
    void on_pushButton_clicked();

private:
    Ui::PictureEdit *ui;

    PhotoShotDialog *m_psDialog;
    QPixmap m_srcPix;
    QPixmap m_scaledPix;

    void loadPicture();

};

#endif // PICTUREEDIT_H
