/********************************************************************************
** Form generated from reading UI file 'facedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FACEDIALOG_H
#define UI_FACEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_FaceDialog
{
public:
    QTableWidget *tableWidget;
    QLabel *label;
    QPushButton *pushButton;

    void setupUi(QDialog *FaceDialog)
    {
        if (FaceDialog->objectName().isEmpty())
            FaceDialog->setObjectName(QStringLiteral("FaceDialog"));
        FaceDialog->resize(453, 432);
        tableWidget = new QTableWidget(FaceDialog);
        if (tableWidget->columnCount() < 12)
            tableWidget->setColumnCount(12);
        if (tableWidget->rowCount() < 11)
            tableWidget->setRowCount(11);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(0, 40, 411, 381));
        tableWidget->setRowCount(11);
        tableWidget->setColumnCount(12);
        tableWidget->horizontalHeader()->setVisible(false);
        tableWidget->horizontalHeader()->setDefaultSectionSize(34);
        tableWidget->horizontalHeader()->setMinimumSectionSize(25);
        tableWidget->verticalHeader()->setVisible(false);
        tableWidget->verticalHeader()->setDefaultSectionSize(34);
        label = new QLabel(FaceDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(0, 0, 151, 41));
        pushButton = new QPushButton(FaceDialog);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(330, 10, 81, 31));

        retranslateUi(FaceDialog);

        QMetaObject::connectSlotsByName(FaceDialog);
    } // setupUi

    void retranslateUi(QDialog *FaceDialog)
    {
        FaceDialog->setWindowTitle(QApplication::translate("FaceDialog", "FaceDialog", nullptr));
        label->setText(QApplication::translate("FaceDialog", "\350\241\250\346\203\205", nullptr));
        pushButton->setText(QApplication::translate("FaceDialog", "\345\205\263\351\227\255\346\214\211\351\222\256", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FaceDialog: public Ui_FaceDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FACEDIALOG_H
