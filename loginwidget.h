#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include "customwidget.h"
#include "clientsocket.h"

namespace Ui {
class loginwidget;
}

/////////////////////////////////////////////////////////////////////////
/// \brief The loginwidget class
/// 登陆界面设计
class loginwidget : public CustomMoveWidget
{
    Q_OBJECT

public:
    explicit loginwidget(QWidget *parent = 0);
    ~loginwidget();

signals:
    void signalRotate();
protected:
    void changeEvent(QEvent *e);
private slots:
    //登录
    void on_btnLogin_clicked();
    // 服务器信息返回处理
    void SltTcpStatus(const quint8 &state);
    void on_btnCancel_clicked();
    void on_btnWinMenu_clicked();
    void SltAnimationFinished();
    void SltEditFinished();
    void on_btnSaveCfg_clicked();

    void on_checkBoxPasswd_clicked(bool checked);

    void on_checkBoxAutoLogin_clicked(bool checked);



    void onSignalMessage(const quint8 &type, const QJsonValue &dataVal);
    //void onSignalStatus(const quint8 &state);

    void on_btnWinClose_clicked();
    void on_btnWinMin_clicked();




private:
    Ui::loginwidget *ui;

    bool m_bConnected;

    //socket通信
    ClientSocket *m_tcpSocket;

    void InitWidget();
};

#endif // LOGINWIDGET_H
