#include "loginwidget.h"
#include "ui_loginwidget.h"
#include "animationstackedwidget.h"
#include "customwidget.h"
#include "mainwindow.h"
#include "clientsocket.h"
#include "unit.h"
#include "global.h"
#include <QFile>
#include <QStackedWidget>
#include <QPropertyAnimation>
#include <QJsonObject>
#include <QCompleter>

loginwidget::loginwidget(QWidget *parent) :
    CustomMoveWidget(parent),
    ui(new Ui::loginwidget)
{
    ui->setupUi(this);

    this->setWindowFlags(Qt::FramelessWindowHint); //去掉Qt自带的标题栏
    this->setAttribute(Qt::WA_TranslucentBackground); //使透明生效
    /*
    QFile file(":/new/prefix1/resource/qss/default.css");
    file.open(QIODevice::ReadOnly);

    //设置样式表
    qApp->setStyleSheet(file.readAll());
    file.close();

    myHelper::setStyle("default.css");

    ui->stackedWidget->setCurrentIndex(0);

    //给lineEditUser添加图片
    ui->lineEditUser->SetIcon(QPixmap(":/new/prefix1/resource/common/ic_user.png"));
    ui->lineEditPasswd->SetIcon(QPixmap(":/new/prefix1/resource/common/ic_lock.png"));
    */
    myHelper::setStyle("default.css");

    InitWidget();

    m_bConnected = false;

    m_tcpSocket = new ClientSocket;
    ui->labelWinTitle->setText("正在连接服务器……");
    m_tcpSocket->ConnectToHost(MyApp::m_strHostAddr, MyApp::m_nMsgPort);
    //connect(m_tcpSocket,&ClientSocket::signalMessage,this,&loginwidget::onSignalMessage);
    //connect(m_tcpSocket,&ClientSocket::signalStatus,this,&loginwidget::onSignalStatus);
    connect(m_tcpSocket, SIGNAL(signalStatus(quint8)), this, SLOT(SltTcpStatus(quint8)));
}

loginwidget::~loginwidget()
{
    delete ui;
}

//changeEvent这个函数作用是什么？？
void loginwidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
/**
 * @brief LoginWidget::SltEditFinished
 * 编辑完成
 */
void loginwidget::SltEditFinished()
{
    QString text = ui->lineEditUser->text();
    if (QString::compare(text, QString("")) != 0) {
           ui->lineEditUser->setText(text);
        qDebug() << "text edit finished" << text;
    }
}

/**
 * @brief LoginWidget::InitWidget
 * 界面初始化
 */
void loginwidget::InitWidget()
{
    // 加载配置信息
    ui->lineEditUser->SetIcon(QPixmap(":/new/prefix1/resource/common/ic_user.png"));
    ui->lineEditUser->setText(MyApp::m_strUserName);

    ui->lineEditPasswd->SetIcon(QPixmap(":/new/prefix1/resource/common/ic_lock.png"));    // 加载之前的配置

    ui->lineEditHostAddr->setText(MyApp::m_strHostAddr);
    ui->lineEditHostMsgPort->setText(QString::number(MyApp::m_nMsgPort));
    ui->lineEditHostFilePort->setText(QString::number(MyApp::m_nFilePort));
    if (ui->checkBoxPasswd->isChecked())
    {
        ui->lineEditPasswd->setText(MyApp::m_strPassword);
    }

    QStringList valueList;
    valueList << "xiaoni" << "zhangsan" << "dengmc" << "wangwu";

    QCompleter *completer = new QCompleter(valueList, this);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    ui->lineEditUser->setCompleter(completer);

    // 信号槽
    connect(ui->lineEditUser, SIGNAL(editingFinished()), this, SLOT(SltEditFinished()));
    connect(ui->lineEditUser, SIGNAL(returnPressed()), this, SLOT(on_btnLogin_clicked()));
    connect(ui->lineEditPasswd, SIGNAL(returnPressed()), this, SLOT(on_btnLogin_clicked()));

    // 默认显示第一界面
    ui->stackedWidget->setCurrentIndex(0);
}
void loginwidget::on_btnWinMenu_clicked()
{
    ui->stackedWidget->setStartVal(0);
    ui->stackedWidget->setEndVal(180);
    //实现动画效果
    ui->stackedWidget->animation(1);

}

/**
 * @brief LoginWidget::SltTcpStatus
 * 读取服务器返回的登陆信息状态--结果
 * @param state
 */
void loginwidget::SltTcpStatus(const quint8 &state)
{
    qDebug()<<"loginwidget::SltTcpStatus";
    switch (state) {
    case DisConnectedHost: {
        m_bConnected = false;
        ui->labelWinTitle->setText(tr("服务器已断开"));
    }
        break;
    case ConnectedHost:
    {
        m_bConnected = true;
        ui->labelWinTitle->setText(tr("已连接服务器"));
        qDebug()<<"login--ConnectedHost--m_strHeadFile:"<<MyApp::m_strHeadFile<<endl;
    }
        break;
        // 登陆验证成功
    case LoginSuccess:
    {
        disconnect(m_tcpSocket, SIGNAL(signalStatus(quint8)), this, SLOT(SltTcpStatus(quint8)));

        // 登录成功后，保存当前用户
        MyApp::m_strUserName = ui->lineEditUser->text();
        MyApp::m_strPassword = ui->lineEditPasswd->text();
        MyApp::SaveConfig();

        // 显示主界面
        MainWindow *mainWindow = new MainWindow();
        qDebug()<<"login--LoginSuccess--m_strHeadFile:"<<MyApp::m_strHeadFile<<endl;
        if (!QFile::exists(MyApp::m_strHeadFile)) {
            qDebug()<<"login-LoginSuccess--exists--m_strHeadFile:"<<MyApp::m_strHeadFile<<endl;
            myHelper::Sleep(100);
            QJsonObject jsonObj;
            jsonObj.insert("from", MyApp::m_nId);
            jsonObj.insert("id",  -2);
            jsonObj.insert("msg", myHelper::GetFileNameWithExtension(MyApp::m_strHeadFile));
            m_tcpSocket->SltSendMessage(GetFile, jsonObj);
            myHelper::Sleep(100);
        }

        // 居中显示
        myHelper::FormInCenter(mainWindow);
        mainWindow->setSocket(m_tcpSocket, MyApp::m_strUserName);
        mainWindow->show();
        this->close();
    }
        break;
    case LoginPasswdError:
    {
        CMessageBox::Infomation(this, "登录失败，请检查用户名和密码！");
    }
        break;
    case LoginRepeat:
    {
        CMessageBox::Infomation(this, "登录失败，该账户已登录！");
    }
        break;

    case RegisterOk:
    {
        CMessageBox::Infomation(this, "该账号注册成功！请点击登录！");
    }
        break;
    case RegisterFailed:
    {
        CMessageBox::Infomation(this, "该账号已经注册！请点击登录！");
    }
        break;
    default:
        break;
    }

    // 还原初始位置，重新登录
    ui->widgetBody->setVisible(true);
    ui->labelUserHead->move(40, 115);
}
void loginwidget::on_btnCancel_clicked()
{
    ui->stackedWidget->setStartVal(0);
    ui->stackedWidget->setEndVal(-180);
    ui->stackedWidget->animation(0);
}
/**
 * @brief loginwidget::on_btnSaveCfg_clicked
 * 保存配置
 */
void loginwidget::on_btnSaveCfg_clicked()
{
    QString strHost = ui->lineEditHostAddr->text();

    // 判断是否ip地址
    if (!myHelper::IsIP(strHost))
    {
        CMessageBox::Infomation(this, tr("IP地址设置有误!"));
        return;
    }

    int nMsgPort = ui->lineEditHostMsgPort->text().toInt();
    if (nMsgPort > 65535 || nMsgPort < 100) {
        CMessageBox::Infomation(this, tr("端口设置有误!"));
        return;
    }

    int nFilePort = ui->lineEditHostFilePort->text().toInt();
    if (nFilePort > 65535 || nFilePort < 100) {
        CMessageBox::Infomation(this, tr("端口设置有误!"));
        return;
    }

    if (nMsgPort == nFilePort) {
        CMessageBox::Infomation(this, tr("2个端口不能设置一样!"));
        return;
    }

    MyApp::m_strHostAddr = strHost;
    MyApp::m_nMsgPort    = nMsgPort;
    MyApp::m_nFilePort   = nFilePort;

    // 保存配置
    MyApp::SaveConfig();

    // 如果没连接服务器，则尝试新的配置连接
    if (!m_bConnected) {
        m_tcpSocket->ConnectToHost(MyApp::m_strHostAddr, MyApp::m_nMsgPort);
    }

    // 返回登录界面
   ui->stackedWidget->setCurrentIndex(0);
}
/**
 * @brief LoginWidget::on_btnLogin_clicked
 * 用户登陆
 */
void loginwidget::on_btnLogin_clicked()
{  
    //检查是否连接到服务器
    m_tcpSocket->CheckConnected();

    QString username = ui->lineEditUser->text();
    QString passwd = ui->lineEditPasswd->text();

    // 构建 Json 对象
    QJsonObject json;
    json.insert("name", username);
    json.insert("passwd", passwd);

    m_tcpSocket->SltSendMessage(0x11,json);

/*
    ui->widgetBody->setVisible(false);

    QPropertyAnimation *animation = new QPropertyAnimation(ui->labelUserHead, "pos");
    animation->setDuration(200);
    animation->setStartValue(QPoint(40, 115));
    animation->setEndValue(QPoint((this->width() - ui->labelUserHead->width()) / 2 - 20, 100));
    connect(animation, SIGNAL(finished()), this, SLOT(SltAnimationFinished()));

    animation->start();
*/
    /*
    //登录 按钮
    MainWindow *mainWindow=new MainWindow;
    mainWindow->show();

    //登录窗口隐藏
    this->hide();
    */
}

void loginwidget::SltAnimationFinished()
{
    if (!m_bConnected) {
        m_tcpSocket->ConnectToHost(MyApp::m_strHostAddr, MyApp::m_nMsgPort);
        CMessageBox::Infomation(this, "未连接服务器，请等待！");
        ui->widgetBody->setVisible(true);
        ui->labelUserHead->move(40, 115);
        return;
    }

    // 构建 Json 对象
    QJsonObject json;
    json.insert("name", ui->lineEditUser->text());
    json.insert("passwd", ui->lineEditPasswd->text());

    m_tcpSocket->SltSendMessage(Login, json);
}
void loginwidget:: onSignalMessage(const quint8 &type, const QJsonValue &dataVal){

}
/*
void loginwidget::onSignalStatus(const quint8 &state){
    qDebug()<<"连接服务器并且登录";
    switch(state){
    case ConnectedHost:
        ui->labelWinTitle->setText("已连接到服务器");
        qDebug()<<"已连接到服务器";
        break;
    case LoginSuccess: //用户登录成功
    {
        disconnect(m_tcpSocket,&ClientSocket::signalStatus,this,&loginwidget::onSignalStatus);
        disconnect(m_tcpSocket,&ClientSocket::signalMessage,this,&loginwidget::onSignalMessage);

        qDebug()<<"用户登录成功";

        //登录按钮  MainWindow显示
        MainWindow *mainWindow=new MainWindow;
        mainWindow->setSocket(m_tcpSocket,ui->lineEditUser->text());
        mainWindow->show();

        //登录窗口隐藏
        this->hide();
    }
        break;
    case LoginPasswdError: //用户未注册
        qDebug()<<"用户未注册";
        break;
    case LoginRepeat: //用户已在线
        qDebug()<<"用户已在线";
        break;
    }
}
*/
void loginwidget::on_btnWinClose_clicked()
{
    this->close();
}

void loginwidget::on_btnWinMin_clicked()
{

}
// 记住密码
void loginwidget::on_checkBoxPasswd_clicked(bool checked)
{
    if (!checked) ui->checkBoxAutoLogin->setChecked(false);
}

void loginwidget::on_checkBoxAutoLogin_clicked(bool checked)
{
    if (checked) ui->checkBoxPasswd->setChecked(true);
}
