#ifndef FACEDIALOG_H
#define FACEDIALOG_H

#include <QDialog>
#include <QList>
#include <QEvent>
#include <QPoint>

#include "emojiitem.h"

namespace Ui {
class FaceDialog;
}

class FaceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FaceDialog(QWidget *parent = 0);
    ~FaceDialog();

    void setSelectFaceIndex(int index);
    int SelectFaceIndex();
    void moveFaceLocation(QPoint p);

private slots:

    void on_btnclose_clicked();

private:
    Ui::FaceDialog *ui;

    int m_selectFaceIndex;
    QList<EmojiItem*> m_emojiList;

    void addEmojiItem(QString fileName);



};

#endif // FACEDIALOG_H
