#include "emojiitem.h"
#include <QMovie>
#include <QEvent>

EmojiItem::EmojiItem(const QString &fileName,QWidget *parent) :
    QLabel(parent)
{
    //gif图片的动态实现
    QMovie *iconMovie = new QMovie;
    iconMovie->setFileName(fileName);
    this->setMovie(iconMovie);
    this->setMargin(4);

    iconMovie->start();
    iconMovie->stop();

    //this->setPixmap(QPixmap(fileName));

}
EmojiItem::~EmojiItem(){

}
void EmojiItem::enterEvent(QEvent *event)
{
    QMovie *iconMovie = this->movie();
    iconMovie->start();

    return QLabel::enterEvent(event);
}
void EmojiItem::leaveEvent(QEvent *event)
{
    QMovie *iconMovie=this->movie();
    iconMovie->jumpToFrame(0);
    iconMovie->stop();

    return QLabel::leaveEvent(event);
}
