#include "clientwidget.h"
#include "ui_clientwidget.h"

#include <QFileDialog>
#include <QDebug>
#include <QDataStream>  //二进制流

ClientWidget::ClientWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClientWidget)
{
    ui->setupUi(this);
    m_bytesWritten = 0;

    m_tcpClient = new QTcpSocket(this);
    connect(m_tcpClient,&QTcpSocket::connected,this,&ClientWidget::onConnected);
    connect(m_tcpClient,&QTcpSocket::bytesWritten,this,&ClientWidget::onBytesWriteen);
}

ClientWidget::~ClientWidget()
{
    delete ui;
}
void ClientWidget::onConnected()
{
    m_localFile = new QFile(m_fileName);
    if(!m_localFile->open(QFile::ReadOnly)){
        qDebug()<<"client:open file error";
        return;
    }

    m_totalBytes = m_localFile->size();  //m_totalBytes文件总大小
    QDataStream sendOut(&m_outBlock,QIODevice::WriteOnly); //第一个参数是数据缓冲区
    sendOut.setVersion(QDataStream::Qt_5_10); //QDataStream是Qt实现的，选择一个QT的版本
    //文件名
    QString curFileName = m_fileName.right(m_fileName.size() - m_fileName.lastIndexOf("/") - 1);
    //发送一个  文件总大小:m_totalBytes  ——   文件名大小  —— 文件名
    sendOut<< qint64(0) <<qint64(0) <<curFileName;
    m_totalBytes += m_outBlock.size();

    //调用QIODevice::seek() ,确保与调用QIODevice的内置缓冲区的完整性
    sendOut.device()->seek(0);

    //文件名的大小：m_outBlock.size() - sizeof(qint64) * 2
    sendOut <<m_totalBytes <<qint64(m_outBlock.size() - sizeof(qint64) * 2);

    m_bytesToWrite = m_totalBytes - m_tcpClient->write(m_outBlock);
    ui->clientMessage->setText("已连接");
    m_outBlock.resize(0);

}
void ClientWidget::onBytesWriteen(qint64 bytes) //bytes写入有效负载的字节数
{
    m_bytesWritten+= (int)bytes;

    if(m_bytesToWrite > 0){
        m_outBlock = m_localFile->read(qMin(m_bytesToWrite,m_playloadSize));
        m_bytesToWrite -= (int)m_tcpClient->write(m_outBlock);

        m_outBlock.resize(0);
    }else{
        m_localFile->close();
    }

    ui->clientProgressBar->setMaximum(m_totalBytes);
    ui->clientProgressBar->setValue(m_bytesWritten);

    if(m_bytesWritten == m_totalBytes){
        ui->clientMessage->setText(QString("传送文件 %1 成功").arg(m_fileName));
        m_localFile->close();
        m_tcpClient->close();
    }

}
void ClientWidget::on_btnOpen_clicked()
{
    ui->clientProgressBar->reset();

    //选择一个文件，并获取文件路径
    m_fileName = QFileDialog::getOpenFileName(this);
    if(!m_fileName.isEmpty()){
        ui->btnSend->setEnabled(true);
        ui->clientMessage->setText(QString("打开文件 %1 成功").arg(m_fileName));
    }


}

void ClientWidget::on_btnSend_clicked()
{
    ui->btnSend->setEnabled(false);
    ui->clientMessage->setText("连接中...");
    m_tcpClient->connectToHost(ui->txtServer->text(),ui->txtPort->text().toInt());  //连接成功后，调用onConnected
}
