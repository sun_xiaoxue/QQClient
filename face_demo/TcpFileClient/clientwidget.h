#ifndef CLIENTWIDGET_H
#define CLIENTWIDGET_H

#include <QWidget>
#include <QTcpSocket>
#include <QFile>

namespace Ui {
class ClientWidget;
}

class ClientWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ClientWidget(QWidget *parent = 0);
    ~ClientWidget();

private:
    Ui::ClientWidget *ui;

    QTcpSocket *m_tcpClient; //套接字
    QFile *m_localFile;      //本地文件（要发送的文件）
    qint64 m_totalBytes;     //文件总大小
    qint64 m_bytesWritten;   //已经发送数据大小
    qint64 m_bytesToWrite;    //剩余数据大小
    QByteArray m_outBlock;    //数据缓冲区
    QString m_fileName;      //文件名
    qint64 m_playloadSize;

private slots:
    void onConnected();
    void onBytesWriteen(qint64 bytes);
    void on_btnOpen_clicked();
    void on_btnSend_clicked();
};

#endif // CLIENTWIDGET_H
