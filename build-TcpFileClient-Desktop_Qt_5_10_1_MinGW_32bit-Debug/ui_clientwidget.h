/********************************************************************************
** Form generated from reading UI file 'clientwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTWIDGET_H
#define UI_CLIENTWIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientWidget
{
public:
    QLabel *lblServer;
    QLabel *lblPort;
    QLineEdit *txtServer;
    QLineEdit *txtPort;
    QProgressBar *clientProgressBar;
    QPushButton *btnOpen;
    QPushButton *btnSend;
    QLabel *clientMessage;

    void setupUi(QWidget *ClientWidget)
    {
        if (ClientWidget->objectName().isEmpty())
            ClientWidget->setObjectName(QStringLiteral("ClientWidget"));
        ClientWidget->resize(360, 242);
        lblServer = new QLabel(ClientWidget);
        lblServer->setObjectName(QStringLiteral("lblServer"));
        lblServer->setGeometry(QRect(50, 50, 81, 31));
        lblPort = new QLabel(ClientWidget);
        lblPort->setObjectName(QStringLiteral("lblPort"));
        lblPort->setGeometry(QRect(50, 90, 71, 16));
        txtServer = new QLineEdit(ClientWidget);
        txtServer->setObjectName(QStringLiteral("txtServer"));
        txtServer->setGeometry(QRect(130, 60, 141, 21));
        txtPort = new QLineEdit(ClientWidget);
        txtPort->setObjectName(QStringLiteral("txtPort"));
        txtPort->setGeometry(QRect(130, 90, 141, 21));
        clientProgressBar = new QProgressBar(ClientWidget);
        clientProgressBar->setObjectName(QStringLiteral("clientProgressBar"));
        clientProgressBar->setGeometry(QRect(50, 160, 271, 21));
        clientProgressBar->setValue(24);
        btnOpen = new QPushButton(ClientWidget);
        btnOpen->setObjectName(QStringLiteral("btnOpen"));
        btnOpen->setGeometry(QRect(50, 190, 91, 31));
        btnSend = new QPushButton(ClientWidget);
        btnSend->setObjectName(QStringLiteral("btnSend"));
        btnSend->setGeometry(QRect(240, 190, 91, 31));
        clientMessage = new QLabel(ClientWidget);
        clientMessage->setObjectName(QStringLiteral("clientMessage"));
        clientMessage->setGeometry(QRect(50, 130, 81, 21));

        retranslateUi(ClientWidget);

        QMetaObject::connectSlotsByName(ClientWidget);
    } // setupUi

    void retranslateUi(QWidget *ClientWidget)
    {
        ClientWidget->setWindowTitle(QApplication::translate("ClientWidget", "ClientWidget", nullptr));
        lblServer->setText(QApplication::translate("ClientWidget", "\346\234\215\345\212\241\345\231\250\345\234\260\345\235\200", nullptr));
        lblPort->setText(QApplication::translate("ClientWidget", "\346\234\215\345\212\241\345\231\250\347\253\257\345\217\243", nullptr));
        txtServer->setText(QApplication::translate("ClientWidget", "127.0.0.1", nullptr));
        txtPort->setText(QApplication::translate("ClientWidget", "12345", nullptr));
        btnOpen->setText(QApplication::translate("ClientWidget", "\346\211\223\345\274\200", nullptr));
        btnSend->setText(QApplication::translate("ClientWidget", "\345\217\221\351\200\201", nullptr));
        clientMessage->setText(QApplication::translate("ClientWidget", "\350\257\267\351\200\211\346\213\251\346\226\207\344\273\266", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ClientWidget: public Ui_ClientWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTWIDGET_H
