#include "linemessage.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

linemessage::linemessage(linemessageData lmData,MessageUser msgUser,QWidget *parent) :
    QWidget(parent)
{
    m_selfHeight=50;

    QHBoxLayout *lmLayout = new QHBoxLayout(this);

    QVBoxLayout *vLayoutTitleAndMsg = new QVBoxLayout(); //vLayout中需要显示时间，消息
    QVBoxLayout *vLayoutHead = new QVBoxLayout();
    QLabel *lblHeadImg = new QLabel(this);
    QWidget *wigHeadImg = new QWidget(this);
    vLayoutHead->addWidget(lblHeadImg);
    vLayoutHead->addWidget(wigHeadImg);

    QWidget *wigTitle = new QWidget();
    QWidget *wigMsg = new QWidget();

    QLabel *lblMsg =new QLabel(wigMsg);
    QLabel *lblTitle = new QLabel(wigTitle);


    QString textTitle = QString("%1 %2").arg(QString("2024/05/27 21:05:30")).arg(QString("Sun"));

    wigTitle->setMinimumWidth(520);
    lblTitle->setText(textTitle);
    lblTitle->setMinimumWidth(520);
    wigMsg->setMinimumWidth(520);

    lblMsg->setText(lmData.message);
    lblMsg->setMinimumWidth(520);

    vLayoutTitleAndMsg->addWidget(wigTitle);
    vLayoutTitleAndMsg->addWidget(wigMsg);

    QFontMetrics fontMet(lblMsg->font());
    int msgFontHeight = fontMet.height(); //获取字体的高度
    int msgFontLeading =fontMet.leading();//行间距

    lblHeadImg->setPixmap(lmData.headImage);//":/images/0.bmp"

    int msgTextHeight = 0;
    if(msgUser == MessageUser_Self){
        msgTextHeight = msgFontHeight*3+msgFontLeading*2;
        lblMsg->setMinimumHeight(msgTextHeight);
        wigMsg->setMinimumHeight(msgTextHeight);

        lmLayout->addLayout(vLayoutTitleAndMsg);
        lmLayout->addLayout(vLayoutHead);

        lblTitle->setAlignment(Qt::AlignRight);
        lblMsg->setAlignment(Qt::AlignRight);
        lblHeadImg->setAlignment(Qt::AlignLeft);


    }else if(msgUser == MessageUser_Friend){

        msgTextHeight = msgFontHeight;
        lblMsg->setMinimumHeight(msgTextHeight);
        wigMsg->setMinimumHeight(msgTextHeight);

        lmLayout->addLayout(vLayoutHead);
        lmLayout->addLayout(vLayoutTitleAndMsg);

        lblTitle->setAlignment(Qt::AlignLeft);
        lblMsg->setAlignment(Qt::AlignLeft);
    }

    this->setStyleSheet("background-color: #FFAAFF");

    m_selfHeight += msgTextHeight;
    this->setGeometry(0,0,620,m_selfHeight);
    this->setMinimumHeight(m_selfHeight);
    this->setMaximumHeight(m_selfHeight);

    this->setLayout(lmLayout);
}

linemessage::~linemessage()
{

}
