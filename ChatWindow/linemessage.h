#ifndef LINEMESSAGE_H
#define LINEMESSAGE_H

#include <QWidget>

//消息用户
typedef enum{
    MessageUser_Self, //自己发送的消息
    MessageUser_Friend, //好友发送的消息
}MessageUser;

typedef struct StLineMessageData{
    QString message; //消息
    QString headImage; //头像
}linemessageData;

class linemessage : public QWidget
{
    Q_OBJECT

public:
    explicit linemessage(linemessageData lmData,MessageUser msgUser = MessageUser_Self,QWidget *parent = 0);
    ~linemessage();

private:
    int m_selfHeight;

};

#endif // LINEMESSAGE_H
