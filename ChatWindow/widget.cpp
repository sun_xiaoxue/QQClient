#include "widget.h"
#include "ui_widget.h"
#include "linemessage.h"
#include <QVBoxLayout>
#include <QHBoxLayout>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    linemessageData lmData;

    m_layout = new QVBoxLayout();

    for(int i =0;i<10;i++) {
        linemessage *lineMsg = NULL;
        if(i%2 == 0){
            lmData.headImage = QString(":/images/0.bmp");
            lmData.message = QString("正在学习C++\n积累经验，找到工作\n加油干，坚持每一天，会有好结果");
            lineMsg = new linemessage(lmData,MessageUser_Self,this);
        }else{
            lmData.headImage = QString(":/images/5.bmp");
            lmData.message = QString("你在干嘛呢");
            lineMsg = new linemessage(lmData,MessageUser_Friend,this);
        }
        m_layout->addWidget(lineMsg);
    }


    ui->scrollArea->widget()->setLayout(m_layout);

}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    linemessageData lmData;
    lmData.headImage = QString(":/images/0.bmp");
    lmData.message = ui->textEdit->toPlainText();
    linemessage* lineMsg = new linemessage(lmData,MessageUser_Self,this);
    m_layout->addWidget(lineMsg);
}
