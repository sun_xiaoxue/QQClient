#include "mainwindow.h"
#include <QApplication>

#include <QFile>
#include <QTextStream>
#include <qapplication.h>

#include "loginwidget.h"
#include "global.h"
#include "databasemagr.h"

// 全局变量
QFile logFile;
QTextStream* logStream = nullptr;

// 日志处理函数
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {
    Q_UNUSED(type);
    Q_UNUSED(context);

    if (logStream) {
        *logStream << msg << endl;
        logStream->flush(); // 确保实时写入文件
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // 打开文件
    logFile.setFileName("debug_output.txt");
    if (!logFile.open(QIODevice::WriteOnly | QIODevice::Append)) {
        qWarning("Could not open debug_output.txt for writing.");
        return -1;
    }

    // 初始化 logStream
    logStream = new QTextStream(&logFile);

    // 安装全局消息处理函数
    qInstallMessageHandler(messageHandler);



    DataBaseMagr::Instance()->OpenUserDb("usr.db");
    DataBaseMagr::Instance()->OpenMessageDb("msg.db");

    loginwidget w;
    myHelper::FormInCenter(&w); //窗体居中，global.h中的函数
    w.show();

    int result = a.exec();

    // 清理
    delete logStream;
    logFile.close();

    return result;
}
