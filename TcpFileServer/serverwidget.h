#ifndef SERVERWIDGET_H
#define SERVERWIDGET_H

#include <QWidget>
#include <QTcpSocket>
#include <QTcpServer>
#include <QFile>


namespace Ui {
class ServerWidget;
}

class ServerWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ServerWidget(QWidget *parent = 0);
    ~ServerWidget();

private slots:
    void onNewConnection();
    void onReadyRead();
    void onError(QAbstractSocket::SocketError error);

    void on_btnStart_clicked();

private:
    Ui::ServerWidget *ui;

    QTcpServer *m_tcpServer;
    QTcpSocket *m_tcpConnection;
    qint64 m_totalBytes;     //文件总大小
    qint64 m_bytesReceived;   //已经接收数据大小
    QByteArray m_inBlock;    //读取数据缓冲区
    qint64 m_fileNameSize;   //文件名大小
    QString m_fileName;      //文件名
    QFile *m_localFile;      //本地文件（要接受的文件）
};

#endif // SERVERWIDGET_H
