#include "serverwidget.h"
#include "ui_serverwidget.h"
#include <QDebug>
#include <QDataStream>
#include <QFile>
ServerWidget::ServerWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ServerWidget)
{
    ui->setupUi(this);

    m_tcpServer = new QTcpServer(this);
    connect(m_tcpServer,&QTcpServer::newConnection,this,&ServerWidget::onNewConnection);

}

ServerWidget::~ServerWidget()
{
    delete ui;
}

void ServerWidget::onNewConnection()
{
    m_tcpConnection = m_tcpServer->nextPendingConnection();
    connect(m_tcpConnection,&QTcpSocket::readyRead,this,&ServerWidget::onReadyRead);
    //connect(m_tcpConnection,&QTcpSocket::error,this,&ServerWidget::onError);
    connect(m_tcpConnection,SIGNAL(erro(QAbstractSocket::SocketError)),this,SLOT(onError(QAbstractSocket::SocketError)));

    ui->serverMessage->setText("接受连接");
    m_tcpServer->close();
}

void ServerWidget::on_btnStart_clicked()
{
    int port=ui->txtPort->text().toInt();
    bool flag = m_tcpServer->listen(QHostAddress::LocalHost,port);
    if(!flag){
        qDebug()<<m_tcpServer->errorString();
        return;
    }

    ui->btnStart->setEnabled(false);
    m_totalBytes = 0;
    m_bytesReceived = 0;
    m_fileNameSize = 0;
    ui->serverMessage->setText("监听");
    ui->serverProgressBar->reset();
}
void ServerWidget::onReadyRead()
{
    QDataStream readIn(m_tcpConnection);
    readIn.setVersion(QDataStream::Qt_5_10);

    //如果接收到数据小于16个字节，保存客户端传来的文件头结构
    if(m_bytesReceived <= sizeof(qint64) * 2){
        if((m_tcpConnection->bytesAvailable() >= sizeof(qint64) * 2)
                && (m_fileNameSize == 0)){
            //接收数据总大小   文件名大小
            readIn >> m_totalBytes >> m_fileNameSize;
            m_bytesReceived += sizeof(qint64) * 2;
        }


        if ((m_tcpConnection->bytesAvailable() >= m_fileNameSize) && m_fileNameSize != 0  )
        {
            //接收文件名，并建立文件
            readIn >> m_fileName;
            ui->serverMessage->setText(QString("接收文件 %1").arg(m_fileName));

            m_bytesReceived += m_fileNameSize;
            m_localFile = new QFile(m_fileName);
            if(! m_localFile ->open(QFile::WriteOnly)){
                qDebug()<< "server:open file error";
                return;
            }
        }else{
            return;
        }
    }
    //接收数据
    if(m_bytesReceived < m_totalBytes){
                qDebug()<<"服务端：接收数据";
        m_bytesReceived += m_tcpConnection->bytesAvailable();
        m_inBlock = m_tcpConnection->readAll();
        m_localFile->write(m_inBlock);
        m_inBlock.resize(0);
    }
    ui->serverProgressBar->setMaximum(m_totalBytes);
    ui->serverProgressBar->setValue(m_bytesReceived);

    //接收数据完成时
    if(m_bytesReceived == m_totalBytes){
        m_tcpConnection->close();
        m_localFile->close();
        ui->btnStart->setEnabled(true);
        ui->serverMessage->setText(QString("接收文件 %1 成功").arg(m_fileName));
    }

}

void ServerWidget::onError(QAbstractSocket::SocketError error)
{

}
