/********************************************************************************
** Form generated from reading UI file 'qqcelltitle.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QQCELLTITLE_H
#define UI_QQCELLTITLE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QQCellTitle
{
public:
    QLabel *labelGroupName;
    QLabel *labelNumber;
    QLabel *labelImage;

    void setupUi(QWidget *QQCellTitle)
    {
        if (QQCellTitle->objectName().isEmpty())
            QQCellTitle->setObjectName(QStringLiteral("QQCellTitle"));
        QQCellTitle->resize(300, 30);
        labelGroupName = new QLabel(QQCellTitle);
        labelGroupName->setObjectName(QStringLiteral("labelGroupName"));
        labelGroupName->setGeometry(QRect(70, 5, 48, 21));
        labelNumber = new QLabel(QQCellTitle);
        labelNumber->setObjectName(QStringLiteral("labelNumber"));
        labelNumber->setGeometry(QRect(219, 5, 41, 21));
        labelImage = new QLabel(QQCellTitle);
        labelImage->setObjectName(QStringLiteral("labelImage"));
        labelImage->setGeometry(QRect(10, 0, 30, 30));
        labelImage->setMinimumSize(QSize(30, 30));
        labelImage->setPixmap(QPixmap(QString::fromUtf8(":/res/aio_arrow_right.png")));

        retranslateUi(QQCellTitle);

        QMetaObject::connectSlotsByName(QQCellTitle);
    } // setupUi

    void retranslateUi(QWidget *QQCellTitle)
    {
        QQCellTitle->setWindowTitle(QApplication::translate("QQCellTitle", "Form", nullptr));
        labelGroupName->setText(QApplication::translate("QQCellTitle", "\346\210\221\347\232\204\345\245\275\345\217\213", nullptr));
        labelNumber->setText(QApplication::translate("QQCellTitle", "[0/0]", nullptr));
        labelImage->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class QQCellTitle: public Ui_QQCellTitle {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QQCELLTITLE_H
