/********************************************************************************
** Form generated from reading UI file 'qqcellline.ui'
**
** Created by: Qt User Interface Compiler version 5.10.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QQCELLLINE_H
#define UI_QQCELLLINE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QQCellLine
{
public:
    QLabel *label;
    QLabel *lblFriendName;
    QLabel *lblStatus;

    void setupUi(QWidget *QQCellLine)
    {
        if (QQCellLine->objectName().isEmpty())
            QQCellLine->setObjectName(QStringLiteral("QQCellLine"));
        QQCellLine->resize(300, 50);
        label = new QLabel(QQCellLine);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 0, 54, 48));
        label->setMinimumSize(QSize(48, 48));
        label->setPixmap(QPixmap(QString::fromUtf8(":/res/head.bmp")));
        lblFriendName = new QLabel(QQCellLine);
        lblFriendName->setObjectName(QStringLiteral("lblFriendName"));
        lblFriendName->setGeometry(QRect(100, 10, 61, 16));
        QFont font;
        font.setFamily(QStringLiteral("Arial"));
        font.setPointSize(12);
        font.setBold(false);
        font.setWeight(50);
        lblFriendName->setFont(font);
        lblStatus = new QLabel(QQCellLine);
        lblStatus->setObjectName(QStringLiteral("lblStatus"));
        lblStatus->setGeometry(QRect(100, 30, 131, 16));

        retranslateUi(QQCellLine);

        QMetaObject::connectSlotsByName(QQCellLine);
    } // setupUi

    void retranslateUi(QWidget *QQCellLine)
    {
        QQCellLine->setWindowTitle(QApplication::translate("QQCellLine", "Form", nullptr));
        label->setText(QString());
        lblFriendName->setText(QApplication::translate("QQCellLine", "Milo", nullptr));
        lblStatus->setText(QApplication::translate("QQCellLine", "\345\275\223\345\211\215\347\224\250\346\210\267\347\212\266\346\200\201\357\274\232\345\234\250\347\272\277", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QQCellLine: public Ui_QQCellLine {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QQCELLLINE_H
