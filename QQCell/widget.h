#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "qqcell.h"

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();
private:
    QQCell *m_cellMyFriend;
    QQCell *m_cellBack;
protected:
    void paintEvent(QPaintEvent *event);
private slots:
    void onCellStatusChange(QQCell *);
};

#endif // WIDGET_H
