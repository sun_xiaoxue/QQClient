#ifndef QQCELL_H
#define QQCELL_H

#include <QWidget>
#include <QList>
#include "qqcelltitle.h"
#include "qqcellline.h"

#define QQ_CELL_WIDTH 300
#define QQ_CELL_TITLE_WIDTH 300
#define QQ_CELL_TITLE_HEIGHT 30
#define QQ_CELL_LINE_WIDTH QQ_CELL_TITLE_WIDTH
#define QQ_CELL_LINE_HEIGHT 50

class QQCell : public QWidget
{
    Q_OBJECT

public:
    explicit QQCell(const QString &GroupTitleName,int offsetCellY = 0,QWidget *parent = 0);
    ~QQCell();

    void addCellFriend(const QString &name,bool isOnline = true);
    void resizeGeometry(int offsetCellY);
signals:
    void sigCellStatusChange(QQCell *);

private slots:
    void onCellStatusChange(bool);

private:
    QQCellTitle *m_cellTitle;
    QWidget *m_cellContent;

    int m_onlineCount;
    int m_groupCount;
    bool m_isOpen; //QQCell组是否展开
    int m_offsetCellY;

    QList<QQCellLine*> m_cellLines; //好友组中的好友个数
};

#endif // QQCELL_H
