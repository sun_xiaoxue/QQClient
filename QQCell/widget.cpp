#include "widget.h"
#include "qqcell.h"

#include <QPainter>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    this->resize(300,600);

    int offsetCellY = 0;
    //我的好友
    m_cellMyFriend = new QQCell(QString("我的好友"),offsetCellY,this);
    m_cellMyFriend->addCellFriend(QString("milo"),true);
    m_cellMyFriend->addCellFriend(QString("mark"),true);

    offsetCellY = m_cellMyFriend->height();
    //黑名单
    m_cellBack= new QQCell(QString("黑名单"),offsetCellY,this);
    m_cellBack->addCellFriend(QString("Lan"),false);

    connect(m_cellMyFriend,&QQCell::sigCellStatusChange,this,&Widget::onCellStatusChange);
    connect(m_cellBack,&QQCell::sigCellStatusChange,this,&Widget::onCellStatusChange);
}

Widget::~Widget()
{

}

void Widget::onCellStatusChange(QQCell *cell){

    if(cell == m_cellMyFriend){
        int offsetCellY = m_cellMyFriend->height();
        //重新设置m_cellBack的偏移量
        m_cellBack->resizeGeometry(offsetCellY);
    }else if(cell == m_cellBack)
    {
        int offsetCellY = m_cellMyFriend->height();
        //重新设置m_cellBack的偏移量
        m_cellBack->resizeGeometry(offsetCellY);
    }
}

void Widget:: paintEvent(QPaintEvent *event){
    QPainter p(this);

    p.setPen(Qt::NoPen);
    p.setBrush(QColor(255,255,255));
    p.drawRect(rect());
}
