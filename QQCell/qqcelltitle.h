#ifndef QQCELLTITLE_H
#define QQCELLTITLE_H

#include <QWidget>

namespace Ui {
class QQCellTitle;
}

class QQCellTitle : public QWidget
{
    Q_OBJECT

public:
    explicit QQCellTitle(const QString &GroupName,QWidget *parent = 0);
    ~QQCellTitle();

    void setOnlineAndGroup(int onlineCount = 0,int groupCount = 0);
signals:
    void sigCellStatusChange(bool);

protected:
    bool eventFilter(QObject *obj, QEvent *event);
    void paintEvent(QPaintEvent *event);

private:
    Ui::QQCellTitle *ui;
    bool m_isOpen;
    bool m_isHover;
};

#endif // QQCELLTITLE_H
