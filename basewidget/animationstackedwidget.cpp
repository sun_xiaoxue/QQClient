#include "animationstackedwidget.h"
#include <QPropertyAnimation>

#include <QPainter>
#include <QPixmap>

AnimationStackedWidget::AnimationStackedWidget(QWidget *parent)
    :QStackedWidget(parent)
{
    m_nextPageIndex = 0;
    m_isAnimating = false;

}

AnimationStackedWidget::~AnimationStackedWidget(){

}

void AnimationStackedWidget::paintEvent(QPaintEvent *event)
{
    if(m_isAnimating){
        QPainter p(this);
        if(iRotateVal>0 && iRotateVal<=90){
            //旋转

            QPixmap pix(currentWidget()->size());
            currentWidget()->render(&pix);

            QTransform transform;
            transform.translate(width()/2,0);
            transform.rotate(iRotateVal,Qt::YAxis); //Qt::YAxis是y轴
            p.setTransform(transform);
            p.drawPixmap(-1*width()/2,0,pix);
        }else if(iRotateVal>90 && iRotateVal<=180){

            QPixmap pix(widget(m_nextPageIndex)->size());
            widget(m_nextPageIndex)->render(&pix);

            QTransform transform;
            transform.translate(width()/2,0);
            transform.rotate(iRotateVal+180,Qt::YAxis); //Qt::YAxis是y轴
            p.setTransform(transform);
            p.drawPixmap(-1*width()/2,0,pix);
        }

        if(iRotateVal>-180 && iRotateVal<=-90){
            //旋转

            QPixmap pix(widget(m_nextPageIndex)->size());
            widget(m_nextPageIndex)->render(&pix);

            QTransform transform;
            transform.translate(width()/2,0);
            transform.rotate(iRotateVal+180,Qt::YAxis); //Qt::YAxis是y轴
            p.setTransform(transform);
            p.drawPixmap(-1*width()/2,0,pix);
        }else if(iRotateVal>-90 && iRotateVal<=0){

            QPixmap pix(currentWidget()->size());
            currentWidget()->render(&pix);

            QTransform transform;
            transform.translate(width()/2,0);
            transform.rotate(iRotateVal,Qt::YAxis); //Qt::YAxis是y轴
            p.setTransform(transform);
            p.drawPixmap(-1*width()/2,0,pix);
        }
    }else{
        QWidget::paintEvent(event);
    }

}

void AnimationStackedWidget::animation(int pageIndex){

    if(m_isAnimating){
        return;
    }

    m_nextPageIndex=pageIndex;

    int offsetX = frameRect().width();
    int offsetY = frameRect().height();
    widget(pageIndex)->setGeometry(0,0,offsetX,offsetY);

    //propertyName
    QPropertyAnimation *animation=new QPropertyAnimation(this,"rotateVal");

    //设置动画持续时间
    animation->setDuration(700);
    //设置动画缓和曲线
    animation->setEasingCurve(QEasingCurve::Linear);
    //设置动画起始值
    animation->setStartValue(m_startVal);
     //设置动画结束值
    animation->setEndValue(m_endVal);

    QObject::connect(animation,&QPropertyAnimation::valueChanged,this,&AnimationStackedWidget::onValueChanged);
    QObject::connect(animation,&QPropertyAnimation::finished,this,&AnimationStackedWidget::onFinished);

    currentWidget()->hide();
    m_isAnimating = true;
    animation->start();
}

void AnimationStackedWidget::onValueChanged()
{
    //值改变了以后
    repaint();  //执行的时候，会调用paintEvent函数
}
void AnimationStackedWidget::onFinished()
{
    m_isAnimating = false;
    //动画结束
    widget(m_nextPageIndex)->show();
    widget(m_nextPageIndex)->raise();

    setCurrentWidget(widget(m_nextPageIndex));
    repaint();
}

